using Prometheus;

namespace DistributedChristmassHouseServer
{
    public static class PromMetrics
    {
        public static readonly Counter Requests = Metrics.CreateCounter("christmas_client_requests", "clients that request an update.", "room");
        public static readonly Counter Samples = Metrics.CreateCounter("christmas_sensor_samples", "samples made by the client", "house_name", "detection_type", "state");
        public static readonly Summary DetectionSummary = Metrics.CreateSummary("christmas_detection", "Detection of life.", "house_name", "detection_type");
        public static readonly Gauge DetectionGauge = Metrics.CreateGauge("christmas_detection_gauge", "Detection of life.", "house_name", "detection_type");
        public static readonly Gauge ActiveGauge = Metrics.CreateGauge("christmas_active", "There is activity", "house_name");
    }
}