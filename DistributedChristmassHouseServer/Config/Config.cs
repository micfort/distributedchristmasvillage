using System.Collections.Generic;

namespace DistributedChristmassHouseServer.Config
{
    public class Config
    {
        public int SoundSensorSampling { get; set; } = 1;
        public int MovementSensorSampling { get; set; } = 1;
        public int ServerUpdateInterval { get; set; } = 1000;
        
        public List<Room> Rooms { get; set; } = new List<Room>();
    }

    public class Room
    {
        public string Name { get; set; }
        public float ThresholdSound { get; set; }
        public float ThresholdMovement { get; set; }
        public string Secret { get; set; }
        public string HttpName { get; set; }
        public Location Location { get; set; }
        
        
        public double KeepAlive { get; set; }
    }

    public enum Location
    {
        Attic = 2,
        TopLeft = 1,
        TopRight = 3,
        BottomLeft = 0,
        BottomRight = 4
    }
}