using Newtonsoft.Json;

namespace DistributedChristmassHouseServer
{
    public class Request
    {
        [JsonProperty("SoundOn")] 
        public int SoundOn { get; set; }

        [JsonProperty("SoundOff")] 
        public int SoundOff { get; set; }

        [JsonProperty("MovementOn")] 
        public int MovementOn { get; set; }

        [JsonProperty("MovementOff")] 
        public int MovementOff { get; set; }
    }
}