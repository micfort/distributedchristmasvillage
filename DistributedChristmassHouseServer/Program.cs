﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using Humanizer;
using Newtonsoft.Json;
using Prometheus;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace DistributedChristmassHouseServer
{
    class Program
    {
        private static readonly ImmutableList<string> Prefixes = new List<string>(){"http://*:8081/"}.ToImmutableList();
        private static readonly Regex Path = new Regex("^/(?<room>[^/]{0,20})/(?<secret>[^/]{20})/?$");

        public static Config.Config Config;
        private static readonly List<Room> Rooms = new List<Room>();

        static void Main(string[] args)
        {
            LoadConfig();
            
            var server = new MetricServer(hostname: "*", port: 8082);
            server.Start();

            if (!HttpListener.IsSupported)
            {
                Console.WriteLine ("Windows XP SP2 or Server 2003 is required to use the HttpListener class.");
                return;
            }
            if (Prefixes == null || Prefixes.Count == 0)
                throw new ArgumentException("prefixes");

            // Create a listener.
            HttpListener listener = new HttpListener();
            // Add the prefixes.
            foreach (string s in Prefixes)
            {
                listener.Prefixes.Add(s);
            }
            listener.Start();
            Console.WriteLine("Listening...");
            while (true)
            {
                // Note: The GetContext method blocks while waiting for a request.
                var context = listener.GetContext();
                var request = context.Request;
                var contentString = new StreamReader(request.InputStream).ReadToEnd();
                var valid = UpdateRooms(request.Url.AbsolutePath, contentString);

                // Obtain a response object.
                var response = context.Response;
                // Construct a response.
                string responseString;
                switch (valid)
                {
                    case Result.Json:
                        var responseObject = new Response()
                        {
                            Leds = GetActiveLeds().ToList(),
                            MovementSensorSampling = Config.MovementSensorSampling,
                            SoundSensorSampling = Config.SoundSensorSampling,
                            ServerUpdateInterval = Config.ServerUpdateInterval,
                        };
                        responseString = JsonConvert.SerializeObject(responseObject);
                        break;
                    case Result.Invalid:
                    default:
                        responseString = "Nothing here";
                        response.StatusCode = 404;
                        PromMetrics.Requests.WithLabels("invalid").Inc();
                        break;
                }
                byte[] buffer = Encoding.UTF8.GetBytes(responseString);
                // Get a response stream and write the response to it.
                response.ContentLength64 = buffer.Length;
                Stream output = response.OutputStream;
                output.Write(buffer,0,buffer.Length);
                // You must close the output stream.
                output.Close();
            }
            listener.Stop();
        }

        static void LoadConfig()
        {
            var deserializer = new DeserializerBuilder()
                .WithNamingConvention(PascalCaseNamingConvention.Instance)
                .Build();

            Config = deserializer.Deserialize<Config.Config>(File.ReadAllText("Config/config.yml"));

            Rooms.AddRange(Config.Rooms.Select(x => new Room(x)));
        }
        
        static Result UpdateRooms(string path, string content)
        {
            var match = Path.Match(path);
            if (!match.Success)
            {
                Console.Out.WriteLine($"Error request to {path}, ignoring");
                return Result.Invalid;
            }
            
            var requestName = match.Groups["room"].Value;
            var requestSecret = match.Groups["secret"].Value;

            var requestedRoom = Rooms.FirstOrDefault(x => 
                string.Equals(x.Config.HttpName, requestName, StringComparison.InvariantCultureIgnoreCase)
                && string.Equals(x.Config.Secret, requestSecret, StringComparison.Ordinal));
            if (requestedRoom == null)
            {
                Console.Out.WriteLine($"Error request to {requestName} with secret {requestSecret}, ignoring");
                return Result.Invalid;
            }

            try
            {
                var request = JsonConvert.DeserializeObject<Request>(content);

                PromMetrics.Samples.WithLabels(requestedRoom.Config.Name, "Movement", "Off").Inc(request.MovementOff);
                PromMetrics.Samples.WithLabels(requestedRoom.Config.Name, "Movement", "On").Inc(request.MovementOn);
                PromMetrics.Samples.WithLabels(requestedRoom.Config.Name, "Sound", "Off").Inc(request.SoundOff);
                PromMetrics.Samples.WithLabels(requestedRoom.Config.Name, "Sound", "On").Inc(request.SoundOn);
                var requestMovement = Convert.ToDouble(request.MovementOn) / (request.MovementOff + request.MovementOn);
                var requestSound = Convert.ToDouble(request.SoundOn) / (request.SoundOff + request.SoundOn);
                Console.Out.WriteLine($"Room request: {requestedRoom.Config.Name} (sound: {requestSound:0.00}, movement: {requestMovement:0.00}, active:{requestedRoom.Active}, time since last update: {(DateTime.Now-requestedRoom.LastUpdated).Humanize(2)})");
                requestedRoom.Update(requestSound, requestMovement);
            }
            catch (Exception)
            {
                Console.Out.WriteLine($"Invalid json request. ignore request, but still send result.");
            }
            
            return Result.Json;
        }
        static IEnumerable<bool> GetActiveLeds()
        {
            return Rooms?.AsEnumerable()
                .OrderBy(x => x.Config.Location)
                .SelectMany(x => Enumerable.Repeat(x.Active, 2))
                .ToList() ?? Enumerable.Empty<bool>();
        }
    }

    public enum Result
    {
        Json,
        Invalid
    }
}