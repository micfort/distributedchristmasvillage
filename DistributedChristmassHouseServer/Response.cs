using System.Collections.Generic;

namespace DistributedChristmassHouseServer
{
    public class Response
    {
        public List<bool> Leds { get; set; }
        public int SoundSensorSampling { get; set; }
        public int MovementSensorSampling { get; set; }
        public int ServerUpdateInterval { get; set; }
    }
}