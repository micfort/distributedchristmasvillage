using System;
using System.Collections.Generic;
using System.Linq;
using Prometheus;

namespace DistributedChristmassHouseServer
{
    internal class Room
    {
        private Queue<Sample> _samples = new Queue<Sample>();

        public Room(Config.Room config)
        {
            Config = config;
            PromMetrics.ActiveGauge.WithLabels(Config.Name).Set(0);
        }

        public Config.Room Config { get; }

        public DateTime LastUpdated { get; private set; } = DateTime.UnixEpoch;

        public bool Active
        {
            get
            {
                Clean();
                if (!_samples.Any())
                {
                    PromMetrics.ActiveGauge.WithLabels(Config.Name).Set(0);
                    return false;
                }
                var active = _samples.Select(x => x.Sound).Where(double.IsFinite).Average() > Config.ThresholdSound ||
                             _samples.Select(x => x.Movement).Where(double.IsFinite).Average() > Config.ThresholdMovement;
                PromMetrics.ActiveGauge.WithLabels(Config.Name).Set(active?1:0);
                return active;
            }
        }
        
        public void Update(double sound, double movement)
        {
            _samples.Enqueue(new Sample(DateTime.Now, sound, movement));
            LastUpdated = DateTime.Now;
            PromMetrics.Requests.WithLabels(Config.Name).Inc();
            PromMetrics.DetectionSummary.WithLabels(Config.Name, "Movement").Observe(movement);
            PromMetrics.DetectionGauge.WithLabels(Config.Name, "Movement").Set(movement);
            PromMetrics.DetectionSummary.WithLabels(Config.Name, "Sound").Observe(sound);
            PromMetrics.DetectionGauge.WithLabels(Config.Name, "Sound").Set(sound);
            PromMetrics.ActiveGauge.WithLabels(Config.Name).Set(Active?1:0);
        }

        public void Clean()
        {
            while (_samples.Any() && DateTime.Now - _samples.Peek().Timestamp > TimeSpan.FromSeconds(Config.KeepAlive))
            {
                _samples.Dequeue();
            }
        }
    }

    public class Sample
    {
        public Sample(DateTime timestamp, double sound, double movement)
        {
            Timestamp = timestamp;
            Sound = sound;
            Movement = movement;
        }

        public DateTime Timestamp { get; }
        public double Sound { get; }
        public double Movement { get; }
    }
}