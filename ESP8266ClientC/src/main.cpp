//
// Created by michiel on 19-12-20.
//

/*
 * Blink
 * Turns on an LED on for one second,
 * then off for one second, repeatedly.
 */

#include "Arduino.h"
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>
#include <FastLED.h>
#include "config.h"

#define NUM_LEDS 10U
#define DATA_PIN 5
#define MOVEMENT_PIN 14
#define SOUND_PIN 13
#define LED_UPDATE_INTERVAL 300
#define INTERVAL_FACTOR 10

CRGB leds[NUM_LEDS];
float ledValues[NUM_LEDS];
bool ledOn[NUM_LEDS];

int MovementSensorSampling = 10;
int SoundSensorSampling = 10;
int ServerUpdateInterval = 10000;
int frameCounter = 0;
int MovementSensorOff = 0;
int MovementSensorOn = 0;
int SoundSensorOff = 0;
int SoundSensorOn = 0;
String url = "";
int ServerFails = 0;

float CalcR(float x)
{
    return roundf(880.0f * x - 1166.667f * powf(x, 2) + 541.6667f * powf(x, 3));
}
float CalcG(float x)
{
    return roundf(189.2857f*x - 208.3333f*powf(x, 2) + 119.0476f*powf(x, 3));
}
float CalcB(float x)
{
    return roundf(21.97619f * x - 31.5f * powf(x, 2) + 19.52381f * powf(x, 3));
}
float RandomF(float max)
{
    return random(1000)/1000.0f*max;
}
float NextNumber(float current, float minValue, float maxValue, float maxStep, float stability)
{
    float real = (current-minValue)/(maxValue-minValue);
    real = max(0.0f, min(1.0f, real));
    float maxStepDown = (powf(real-1, stability)-1)*maxStep;
    float maxStepUp = (1-powf(real, stability))*maxStep;
    current += RandomF(maxStepUp-maxStepDown)+maxStepDown;
    return current;
}
float NextNumberToZero(float current, float maxStep)
{
    if(current == 0)
        return 0;
    current -= RandomF(maxStep);
    if(current < 0)
        return 0;
    return current;
}

String BuildJSON(int sound_on, int sound_off, int movement_on, int movement_off)
{
    return "{SoundOn:"+String(sound_on)+",SoundOff:"+String(sound_off)+",MovementOn:"+String(movement_on)+",MovementOff:"+String(movement_off)+"}";
}

void SetAllLeds(uint8_t nr, uint8_t ng, uint8_t nb)
{
    for (int i = 0; i < NUM_LEDS; ++i)
    {
        leds[i].setRGB(nr, ng, nb);
    }
    FastLED.show();
}

void setup()
{
    url = String(HTTP_BASE) + "/" + HTTP_NAME + "/" + HTTP_SECRET;

    Serial.begin(115200);

    FastLED.addLeds<WS2811, DATA_PIN>(leds, NUM_LEDS);
    for (unsigned i = 0; i < NUM_LEDS; ++i) {
        leds[i] = CRGB::Black;
        ledValues[i] = 0;
        ledOn[i] = false;
    }

    WiFi.mode(WIFI_STA);
    int cnt = 0;
    while(WiFi.status() != WL_CONNECTED)
    {
        SetAllLeds(0, 0, 255);
        delay(250);
        SetAllLeds(0, 0, 0);
        delay(250);
        if(cnt++ >= 10)
        {
            WiFi.beginSmartConfig();
            while(!WiFi.smartConfigDone())
            {
                SetAllLeds(255, 0, 0);
                FastLED.show();
                delay(500);
                SetAllLeds(0, 0, 0);
                delay(500);
            }
            Serial.println("SmartConfig Success");
        }
    }

    Serial.print("Connected to");
    Serial.println(WiFi.SSID());

    SetAllLeds(0, 255, 0);
    delay(1000);
    SetAllLeds(0, 0, 0);
}

void UpdateServer(int frame)
{
    if(frame%ServerUpdateInterval != 0) return;
    if (WiFi.status() != wl_status_t::WL_CONNECTED) return;
    DynamicJsonDocument doc(1024);
    HTTPClient http;

    Serial.print(url);
    Serial.print("\r\n");
    http.begin(url);
    auto request = BuildJSON(SoundSensorOn, SoundSensorOff, MovementSensorOn, MovementSensorOff);
    SoundSensorOn = 0;
    SoundSensorOff = 0;
    MovementSensorOn = 0;
    MovementSensorOff = 0;
    Serial.print(request);
    Serial.print("\r\n");
    int httpCode = http.POST(request);

    if (httpCode == 200)
    {
        ServerFails = 0;
        String payload = http.getString();
        Serial.print(payload);
        Serial.print("\r\n");
        deserializeJson(doc, payload);
        JsonObject obj = doc.as<JsonObject>();
        MovementSensorSampling = obj["MovementSensorSampling"].as<int>()*INTERVAL_FACTOR;
        SoundSensorSampling = obj["SoundSensorSampling"].as<int>()*INTERVAL_FACTOR;
        ServerUpdateInterval = obj["ServerUpdateInterval"].as<int>()*INTERVAL_FACTOR;
        auto leds = obj["Leds"];
        for (unsigned i = 0; i < min(leds.size(), NUM_LEDS); ++i) {
            ledOn[i] = leds[i].as<bool>();
        }
    }
    else
    {
        ServerFails++;
        if(ServerFails > 20)
        {
            for (unsigned i = 0; i < NUM_LEDS; ++i) {
                ledOn[i] = true;
            }
        }
    }
    http.end();   //Close connection
}

void UpdateLeds(int frame)
{
    if(frame%LED_UPDATE_INTERVAL != 0) return;
    for (unsigned i = 0; i < NUM_LEDS; ++i)
    {
        if(ledOn[i])
        {
            ledValues[i] = NextNumber(ledValues[i], 0.4f, 1.0f, 0.1f, 4.0f);
        }
        else
        {
            ledValues[i] = NextNumberToZero(ledValues[i], 0.1f);
        }
        leds[i].setRGB(CalcR(ledValues[i]), CalcG(ledValues[i]), CalcB(ledValues[i]));
        FastLED.show();
    }
}

void UpdateSensors(int frame)
{
    if(frame%MovementSensorSampling == 0)
    {
        if(digitalRead(MOVEMENT_PIN))
            MovementSensorOn++;
        else
            MovementSensorOff++;
    }
    if(frame%SoundSensorSampling == 0)
    {
        if(!digitalRead(SOUND_PIN))
            SoundSensorOn++;
        else
            SoundSensorOff++;
    }
}

void loop()
{
    if(frameCounter%MovementSensorSampling == 0
    && frameCounter%SoundSensorSampling == 0
    && frameCounter%LED_UPDATE_INTERVAL == 0
    && frameCounter%ServerUpdateInterval == 0)
        frameCounter = 0;

    UpdateServer(frameCounter);
    UpdateLeds(frameCounter);
    UpdateSensors(frameCounter);
    frameCounter++;
    delayMicroseconds(100);
}