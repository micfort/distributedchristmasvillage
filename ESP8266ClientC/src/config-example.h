#ifndef ESP8266CLIENTC_CONFIG_H
#define ESP8266CLIENTC_CONFIG_H

#define HTTP_BASE "http://your-host"
#define HTTP_NAME "name-of-node"
#define HTTP_SECRET "some-random-secret-20-characters-long"

#endif //ESP8266CLIENTC_CONFIG_EXAMPLE_H
