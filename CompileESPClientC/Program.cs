﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using DistributedChristmassHouseServer.Config;
using Stubble.Core.Builders;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace CompileESPClientC
{
    class Program
    {
        static void Main(string[] args)
        {
            var deserializer = new DeserializerBuilder()
                .WithNamingConvention(PascalCaseNamingConvention.Instance)
                .Build();

            var config = deserializer.Deserialize<Config>(File.ReadAllText(args[0]));

            foreach (var templateConfig in config.Rooms.Select(x => new TemplateConfig(args[1], x)))
            {
                var stubble = new StubbleBuilder().Build();
                using StreamReader streamReader = new StreamReader($@"{AssemblyDirectory}/configH.mustache", Encoding.UTF8);
                var output = stubble.Render(streamReader.ReadToEnd(), templateConfig);
                File.WriteAllText("src/config.h", output);

                var compile = Process.Start("platformio", "run");
                compile.WaitForExit();

                Directory.CreateDirectory($"Firmwares/{templateConfig.HttpName}");
                File.Copy(".pio/build/d1/firmware.bin", $"Firmwares/{templateConfig.HttpName}/firmware.bin", true);
                File.Copy(".pio/build/d1/firmware.elf", $"Firmwares/{templateConfig.HttpName}/firmware.elf", true);
                File.Copy($@"{AssemblyDirectory}/program.sh", $"Firmwares/{templateConfig.HttpName}/program.sh", true);
                File.Copy($@"{AssemblyDirectory}/program.bat", $"Firmwares/{templateConfig.HttpName}/program.bat", true);
            }
        }
        
        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }
    }

    class TemplateConfig
    {
        public string HttpName { get; set; }
        public string HttpSecret { get; set; }
        public string BaseHttp { get; set; }
        
        public TemplateConfig(string baseHttp, Room room)
        {
            HttpName = room.HttpName;
            HttpSecret = room.Secret;
            BaseHttp = baseHttp;
        }
    }
}