# DistributedChristmasVillage

This project is a project for the COVID-19 time. Christmas is different than other years, so I have created a number of houses with lights. One of these lights will light up when a person talks or moves before the house. Because the houses are connected to a server, not only the light in their house will light up, but also the specific light in the other houses will light up. So these houses are synchronized, to create a bit of togetherness between people.

The image below shows an small house with 3 rooms light up. The people behind the windows is cardboard that blocks the light. These depict the people living in where the house stands.

![alt text](Documentation/HousePicture.jpg "An small 3d printed house with 3 windows on.")

## 3D models

The 3D models are listed in this repository. These are offcourse without any color. I have printed them black, so that the light doesn't go from the one room to the other. Also the windows are milled out of acrilic. That gave a better result in the lighting.

## Electronics

![alt text](Documentation/Electronic Overview.png "An overview of the electronics.")

The electronics are shown in the diagram. I have used the following:

 - Wemos Lolin D1 mini (but the USB to UART converter removed)
 - a PIR movement sensor
 - a sound sensor (I used one that only detects sound, not that it will actually hear sound. This to solve the issue that it should not and can not be used for spying)
 - a level converter (to solve the issue that he leds are 5v and the ESP8266 is 3.3v)
 - 10 WS2811 LEDs
 - Standard 5v USB Power supply

Some notes about the electronics:

 - I split up the 5v before going into the Wemos Lolin D1 mini. This is done because there is an resistor on the board, and the LEDs took too much power.
 - My sensors had problems with close by wifi, this resulted in the problem that they needed to be shielded. This took some work, but in the end was successful.
 - The Dupont connectors are neccesary, because otherwise the system was not possible to assemble.
 - The empty dupont connector can be used to reprogram the houses, but for that to work the usb to uart converter on the Wemos board should be removed.

## Software

The software is divided in to 2 parts, server and clients.

Some notes on the software:
 - The decision which LED should light up is done by the server. This results that configuring the thresholds is easier by the maintainer.
 - The communication between the server and clients is done by a simple HTTP request with JSON content.
 - To test the system when not all houses are finished, there is a ClientSimulator application build with AvaloniaUI to simulate the houses. This UI also works on Linux.

### Server

The server is basically a very simple HTTP server. That first validates the name and secret of the houses. Then it will try to deserialize the json request. After which it sends which LEDs should light up. The server can be configured with a small yaml file. Also a docker image is created to make the deployment easy for future use.

A second rest interface is created for Prometheus metrics. This is mostly used to check if the system works correctly.

### Client

The client is written for the ESP8266 with the Arduino Framework. The environment used is PlatformIO. This results in an easy way to compile 5 different firmwares. One firmware for every house. The clients als use a random method to create a small flickering in the light. Also the lights do not turn on/off immediately, but slowly fade.

There is an extra C# application that creates the firmwares for the houses. These are distilled from a server configuration file combined with a base http url. It uses the platformio cli on the path variable to compile the firmwares. It should be executed in the ESP8266ClientC directory.