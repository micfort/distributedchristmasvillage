using Avalonia;
using Avalonia.Markup.Xaml;

namespace avaloniaTEst
{
    public class App : Application
    {
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}