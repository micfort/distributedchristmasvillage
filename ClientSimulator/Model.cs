using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using Avalonia.Threading;
using DistributedChristmassHouseServer;
using DistributedChristmassHouseServer.Config;
using Newtonsoft.Json;
using ReactiveUI;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace ClientSimulator
{
    public class Model: ReactiveObject
    {
        private string _server = "http://localhost:8081";
        public List<ModelHouse> Houses { get; set; }

        public string Server
        {
            get => _server;
            set => this.RaiseAndSetIfChanged(ref _server, value);
        }

        public Model()
        {
            var deserializer = new DeserializerBuilder()
                .WithNamingConvention(PascalCaseNamingConvention.Instance)
                .Build();

            var config = deserializer.Deserialize<Config>(File.ReadAllText("Config/config.yml"));

            Houses = config.Rooms.Select(x => new ModelHouse(this, x.Name, x.HttpName, x.Secret)).ToList();
        }
    }

    public class ModelHouse: ReactiveObject
    {
        private readonly Model _model;
        private static Random _random = new Random();
        
        private float _movement;
        private float _sound;
        private bool _enabled = false;
        
        private DispatcherTimer timer;
        private DispatcherTimer startUpTimer;
        private bool _randomized;

        public ModelHouse(Model model, string name, string httpName, string secret)
        {
            Name = name;
            HttpName = httpName;
            Secret = secret;
            _model = model;
            timer = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, updateEvent);
        }
        
        private void updateEvent(object? sender, EventArgs e)
        {
            Update();
        }

        public string Name { get; }
        public string HttpName { get; }
        public string Secret { get; }
        
        // ReSharper disable once InconsistentNaming
        public ObservableCollection<LedStatus> LEDs { get; set; } = new ObservableCollection<LedStatus>(Enumerable.Repeat(LedStatus.Unknown, 10));

        public float Movement
        {
            get => _movement;
            set => this.RaiseAndSetIfChanged(ref _movement, value);
        }

        public float Sound
        {
            get => _sound;
            set => this.RaiseAndSetIfChanged(ref _sound, value);
        }

        public bool Enabled
        {
            get => timer.IsEnabled;
            set
            {
                if(timer.IsEnabled == value) return;
                this.RaisePropertyChanged();
                timer.IsEnabled = value;
            }
        }

        public bool Randomized
        {
            get => _randomized;
            set => this.RaiseAndSetIfChanged(ref _randomized, value);
        }

        public void Update()
        {
            if (_randomized)
            {
                Sound = Convert.ToSingle(_random.NextDouble());
                Movement = 0;
            }
            try
            {
                WebClient client = new WebClient();
                var url = $"{_model.Server}/{HttpName}/{Secret}";
                Console.Out.WriteLine($"Updating with url: {url}");
                var request = new Request
                {
                    MovementOff = Convert.ToInt32(Math.Round(100 * (1 - Movement))),
                    MovementOn = Convert.ToInt32(Math.Round(100 * Movement)),
                    SoundOff = Convert.ToInt32(Math.Round(100 * (1 - Sound))),
                    SoundOn = Convert.ToInt32(Math.Round(100 * Sound))
                };

                var result = client.UploadString(url, JsonConvert.SerializeObject(request));
                var response = JsonConvert.DeserializeObject<Response>(result);
                if (response.Leds.Count <= LEDs.Count)
                {
                    for (var i = 0; i < response.Leds.Count; i++)
                    {
                        LEDs[i] = response.Leds[i]?LedStatus.On:LedStatus.Off;
                    }
                    for (var i = response.Leds.Count; i < LEDs.Count; i++)
                    {
                        LEDs[i] = LedStatus.Unknown;
                    }
                }
                else
                {
                    for (var i = 0; i < LEDs.Count; i++)
                    {
                        LEDs[i] = response.Leds[i]?LedStatus.On:LedStatus.Off;
                    }
                }
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}