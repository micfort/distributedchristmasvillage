namespace ClientSimulator
{
    public enum LedStatus
    {
        Off,
        On,
        Unknown
    }
}