using System;
using System.Globalization;
using Avalonia.Data.Converters;
using Avalonia.Media;

namespace ClientSimulator
{
    public class LedConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value == null) return new SolidColorBrush(Colors.Red);
            if(!(value is LedStatus ledStatus)) return new SolidColorBrush(Colors.Red);
            switch (ledStatus)
            {
                case LedStatus.Off:
                    return new SolidColorBrush(Colors.Black);
                case LedStatus.On:
                    return new SolidColorBrush(Colors.LightYellow);
                case LedStatus.Unknown:
                    return new SolidColorBrush(Colors.Red);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}