using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace ClientSimulator
{
    public class House : UserControl
    {
        public House()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

    }
}